!1 End-to-End Tests Guarabot

Estas pruebas son de punta a punta y para su ejecución es necesario utilizar tanto el Bot como la API Rest.

Escenario e2e1: Inscripcion exitosa
Dada la materia "Memo2" con codigo "9521" y 10 cupos
Cuando @juanperez envia "/inscribir 9521"
Entonces recibe "inscripcion ok"
Y @juanperez esta inscripto en "9521"
Y "9521" tiene 1 inscripto
Y "9521" tiene 9 cupos disponibles

Escenario e2e2: Consulta de oferta
Dada la materia "Algo1" con codigo "7540" y 20 cupos
Y la materia "Algo2" con codigo "7541" y 10 cupos
Y la materia "Algo3" con codigo "7507" y 5 cupos
Cuando @juanperez envia "/oferta"
Entonces recibe:
    7540:Algo1
    7541:Algo2
    7507:Algo3
